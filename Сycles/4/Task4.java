package com.company;
import java.util.Scanner;
public class Task4 {

    public static void main(String[] args) {
        System.out.println("Вычислить факториал числа n.");
        System.out.println(fac(10));
    }


    public static int fac(Integer num){
        System.out.println(num);
        int result = 1;
        for (int i = 1; i <= num; i++) {
            result = result * i;
        }
        return result;
    }
}
