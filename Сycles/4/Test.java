import com.company.Task4;
import org.junit.Assert;

public class Test {


    @org.junit.Test
    public void test() {
        Task4 task1 = new Task4();
        int result = task1.fac(5);
        Assert.assertEquals(120, result);

        int result1 = task1.fac(10);
        Assert.assertEquals(3628800, result1);

    }
}
