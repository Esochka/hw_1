package com.company;

import java.util.Scanner;

public class Task3 {

    public static void main(String[] args) {

        System.out.println("Найти корень натурального числа с точностью до целого");
        System.out.println("последовательного подбора");
        System.out.println(korPos(9));
        System.out.println("метод бинарного поиска)");
        System.out.println(korBin(9));

    }

    public static int korPos(int n) {
        System.out.println(n);
        int i = 0, result = 0;
        while (result <= n) {
            i++;
            result = i * i;
        }
        return i - 1;
    }

    public static int korBin(int n) {
        System.out.println(n);

        if (n == 0 || n == 1) {
            return n;
        } else {
            int start = 1, end = n / 2, mid = start + (end - start) / 2;
            while (start <= end) {
                mid = start + (end - start) / 2;

                if (mid <= n / mid) {
                    start = mid + 1;


                } else {
                    end = mid - 1;
                }
            }
            if (mid == n / mid) {
                return mid;
            } else {
                return start - 1;
            }
        }

    }
}
