import com.company.Task3;
import org.junit.Assert;
public class Test2 {
    @org.junit.Test
    public void task2() {
        Task3 task2 = new Task3();
        int result = task2.korBin(5);
        Assert.assertEquals(2, result);

        int result1 = task2.korBin(9);
        Assert.assertEquals(3, result1);

    }

}