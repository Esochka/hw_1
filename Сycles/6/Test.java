import com.company.Task6;
import org.junit.Assert;

public class Test {


    @org.junit.Test
    public void test() {
        Task6 task1 = new Task6();
        String result = task1.revers("12345");
        Assert.assertEquals("54321", result);

        String result1 = task1.revers("123456");
        Assert.assertEquals("654321", result1);

    }
}
