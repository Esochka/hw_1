package com.company;

public class Task2 {

    public static void main(String[] args) {

        System.out.println("Проверить простое ли число?");
        System.out.println(check(10));

    }

    public static String check(int num) {
        int temp;
        boolean isPrime = true;
        for (int i = 2; i <= num - 1; i++) {
            temp = num % i;
            if (temp == 0) {
                isPrime = false;
                break;
            }
        }
        if (isPrime) {
            return ""+num+" простое число";
        } else {
            return ""+num+" составное число";
        }


    }
}
