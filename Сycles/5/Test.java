import com.company.Task5;
import org.junit.Assert;

public class Test {


    @org.junit.Test
    public void test() {
        Task5 task1 = new Task5();
        int result = task1.sum("1234");
        Assert.assertEquals(10, result);

        int result1 = task1.sum("12345");
        Assert.assertEquals(15, result1);

    }
}
