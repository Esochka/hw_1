package com.company;

public class Task5 {

    public static void main(String[] args) {
        System.out.println("Посчитать сумму цифр числа");
        System.out.println(sum("1234"));
    }


    public static int sum(String s) {
        System.out.println(s);

        String[] a = s.split("");
        int m = 0;
        for (int i = 0; i < a.length; i++) {
            m = m + Integer.parseInt(a[i]);
        }
        return m;

    }
}
