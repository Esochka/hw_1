package com.company;

import java.util.Scanner;

public class Task1 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Вводим число(0-999), получаем строку с прописью числа.");
        System.out.println("Введите число от 0 до 999");


        if (scanner.hasNextInt()) {
            int a = scanner.nextInt();
            System.out.println(res(a));
        } else {
            System.out.println("Wrong data!");
        }


    }

    public static String res(Integer a) {
        if (a > 999 || a < 0) {
            return "999<число<0";
        } else if (a == 0) {
            return "Ноль";

        } else {
            return revers(a);

        }


    }

    public static String revers(int a) {
        String[] result = {"", "", ""};

        int[] numbers = {1, 2, 3, 4, 5, 6, 7, 8, 9};
        String[] num1 = {"один", "два", "три", "четыре", "пять", "шесть", "семь", "восемь", "девять",};
        String[] num10 = {"десять", "двадцать", "тридцать", "сорок", "пятьдесят", "шестьдесят", "семьдесят", "восемьдесят", "девяносто",};
        String[] num11 = {"одиннадцать", "двенадцать", "тринадцать", "четырнадцать", "пятнадцать", "шестнадцать", "семнадцать", "восемнадцать", "девятнадцать"};
        String[] num100 = {"сто", "двести", "триста", "четыреста", "пятьсот", "шестьсот", "семьсот", "восемьсот", "девятьсот"};


        int k = 1;
        while (a >= 1) {
            int b = a % 10;
            a = a / 10;
            for (int i = 0; i < 9; i++) {
                if (numbers[i] == b) {
                    if (k == 1) {
                        result[2] = " " + num1[i];
                        if (a % 10 == 1) {
                            result[2] = " " + num11[i];
                            a /= 10;
                            k++;
                        }
                    } else if (k == 2) {
                        result[1] = " " + num10[i];

                    } else if (k == 3) {
                        result[0] = num100[i];
                    }
                }
            }

            k++;
        }


        StringBuilder res = new StringBuilder();
        for (int i = 0; i < result.length; i++) {
            res.append(result[i]);
        }
        return res.toString().trim();
    }


}
