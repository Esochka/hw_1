import com.company.Task5;
import org.junit.Assert;

public class Test {


    @org.junit.Test
    public void test() {
        Task5 task1 = new Task5();
        String result = (String) task1.revers_bigger(100);
        Assert.assertEquals("сто", result);

        String result1 = (String) task1.revers_bigger(0);
        Assert.assertEquals("Ноль", result1);

        String result2 = (String) task1.revers_bigger(102);
        Assert.assertEquals("сто два", result2);

    }
}
