package com.company;

import java.io.Serializable;
import java.util.Scanner;

public class Task5 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Вводим число(0-999 миллиардов), получаем строку с прописью числа.");
        System.out.println("Введите число от 0 до 999000000");

        if (scanner.hasNextInt()) {
            long a = scanner.nextInt();
            System.out.println(revers_bigger(a));
        } else {
            System.out.println("Wrong data!");
        }


    }

    public static Serializable revers_bigger(long a) {
        int i = 0;
        String[] words = {"", "", "", "", "", "", "", "", "", "", "", "", ""};
        if (a == 0) {

            words = new String[]{"Ноль", " "};
        }
        int t = 11;
        while (a > 0) {
            long b = a % 1000;
            String[] c = {"", "", ""};
            c = revers(b);
            i++;
            a /= 1000;
            switch (i) {
                case 2:
                    c[2] = c[2] + " тысяч ";
                    break;
                case 3:
                    c[2] = c[2] + " миллиона ";
                    break;
                case 4:
                    c[2] = c[2] + " миллиардов ";
            }
            for (int j = 2; j >= 0; j--) {
                words[t] = c[j];
                t--;
            }
        }
        StringBuilder res = new StringBuilder();
        for (i = 0; i < words.length; i++) {
            res.append(words[i]);
        }
        return res.toString().trim();
    }

    public static String[] revers(long a) {

        String[] result = {"", "", ""};

        int[] numbers = {1, 2, 3, 4, 5, 6, 7, 8, 9};
        String[] num1 = {"один", "два", "три", "четыре", "пять", "шесть", "семь", "восемь", "девять",};
        String[] num10 = {"десять", "двадцать", "тридцать", "сорок", "пятьдесят", "шестьдесят", "семьдесят", "восемьдесят", "девяносто",};
        String[] num11 = {"одиннадцать", "двенадцать", "тринадцать", "четырнадцать", "пятнадцать", "шестнадцать", "семнадцать", "восемнадцать", "девятнадцать"};
        String[] num100 = {"сто", "двести", "триста", "четыреста", "пятьсот", "шестьсот", "семьсот", "восемьсот", "девятьсот"};


        int k = 1;
        while (a >= 1) {
            long b = a % 10;
            a = a / 10;
            for (int i = 0; i < 9; i++) {
                if (numbers[i] == b) {
                    if (k == 1) {
                        result[2] = " " + num1[i];
                        if (a % 10 == 1) {
                            result[2] = " " + num11[i];
                            a /= 10;
                            k++;
                        }
                    } else if (k == 2) {
                        result[1] = " " + num10[i];

                    } else if (k == 3) {
                        result[0] = num100[i];
                    }
                }
            }

            k++;
        }

        return result;
    }


}
