package com.company;

import java.util.Scanner;
import java.util.*;


public class Task4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Вводим строку, которая содержит число, написанное прописью (0-999 миллиардов). Получить само\n" +
                "число");
        System.out.println("Введите число прописью (от 0 до 999 миллиардов (слова в нижнем регистре))");
        String words = scanner.nextLine();

        words = words + " .";


        String[] word = words.split(" ");

        System.out.println(rez(word));


        System.out.println("Если вывод неправильный то не правильное написание слов");

    }

    public static String rez(String[] word){
        boolean hasMlrd = false;
        boolean hasMln = false;
        boolean hasTus = false;
        String rev = "";

        if (Arrays.asList(word).contains("миллиардов")) {
            hasMlrd = true;
        }
        if (Arrays.asList(word).contains("миллионов")) {
            hasMln = true;
        }
        if (Arrays.asList(word).contains("тысяч")) {
            hasTus = true;
        }


        StringBuilder beforeMlrd = new StringBuilder();
        StringBuilder beforeMln = new StringBuilder();
        StringBuilder beforeTus = new StringBuilder();
        StringBuilder hundredth = new StringBuilder();

        int k = 0;

        if (hasMlrd) {
            k = 0;
            while (!word[k].equals("миллиардов")) {
                beforeMlrd.append(word[k]).append(" ");
                k++;
            }

        }


        if (hasMln) {
            while (!word[k].equals("миллионов")) {
                beforeMln.append(word[k]).append(" ");
                k++;
            }
        }
        if (hasTus) {
            while (!word[k].equals("тысяч")) {
                beforeTus.append(word[k]).append(" ");
                k++;
            }
        }

        while (!word[k].equals(".")) {
            hundredth.append(word[k]).append(" ");
            k++;
        }


        String[] arrMlrd = beforeMlrd.toString().split(" ");
        String[] arrMln = beforeMln.toString().split(" ");
        String[] arrTus = beforeTus.toString().split(" ");
        String[] arrHundredth = hundredth.toString().split(" ");


        int[] mlrd = newArray(arrMlrd);
        int[] mln = newArray(arrMln);
        int[] tus = newArray(arrTus);
        int[] hndt = newArray(arrHundredth);

        for (int i = 0; i < 3; i++) {

            rev = rev + mlrd[i];
        }
        for (int i = 0; i < 3; i++) {

            rev = rev + mln[i];

        }
        for (int i = 0; i < 3; i++) {

            rev = rev + tus[i];

        }
        for (int i = 0; i < 3; i++) {

            rev = rev + hndt[i];

        }
        return rev;
    }

    public static int[] newArray(String[] array) {
        int[] number = {0, 0, 0};
        int[] numbers = {1, 2, 3, 4, 5, 6, 7, 8, 9};
        String[] num1 = {"один", "два", "три", "четыре", "пять", "шесть", "семь", "восемь", "девять"};
        String[] num10 = {"десять", "двадцать", "тридцать", "сорок", "пятьдесят", "шестьдесят", "семьдесят", "восемьдесят", "девяносто"};
        String[] num11 = {"одиннадцать", "двенадцать", "тринадцать", "четырнадцать", "пятнадцать", "шестнадцать", "семнадцать", "восемнадцать", "девятнадцать"};
        String[] num100 = {"сто", "двести", "триста", "четыреста", "пятьсот", "шестьсот", "семьсот", "восемьсот", "девятьсот"};

        for (int i = 0; i < array.length; i++)
            for (int j = 0; j < 9; j++) {
                if (array[i].equals(num1[j])) {
                    number[2] = numbers[j];
                }
                if (array[i].equals(num10[j])) {
                    number[1] = numbers[j];
                }
                if (array[i].equals(num11[j])) {
                    number[2] = j + 1;
                    number[1] = 1;
                }
                if (array[i].equals(num100[j])) {
                    number[0] = numbers[j];
                }

            }
        return number;
    }


}