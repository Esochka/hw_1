import com.company.Task4;
import org.junit.Assert;

public class Test {


    @org.junit.Test
    public void test() {
        Task4 task1 = new Task4();
        String result = Task4.rez(new String[]{"сто ."});
        Assert.assertEquals("000000000100", result);

        String result2 = Task4.rez(new String[]{"один тысяч два ."});
        Assert.assertEquals("000000001002", result2);

    }
}
