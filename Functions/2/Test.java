import com.company.Task3;
import org.junit.Assert;

public class Test {


    @org.junit.Test
    public void test() {
        Task3 task1 = new Task3();
        String result = task1.newArray("сто два");
        Assert.assertEquals("102", result);

        String result2 = task1.newArray("один");
        Assert.assertEquals("001", result2);

    }
}
