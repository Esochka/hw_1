package com.company;

public class Task5 {

    public static void main(String[] args) {

        System.out.println("Написать программу определения оценки студента по его рейтингу");
        System.out.println(rating(30));


    }

    public static String rating(int x) {
        if (x >= 0 && x <= 19) {

            return "Оценка F ";
        } else if (x >= 20 && x <= 39) {

            return "Оценка E ";
        } else if (x >= 40 && x <= 59) {

            return "Оценка D ";
        } else if (x >= 60 && x <= 74) {

            return "Оценка C ";
        } else if (x >= 75 && x <= 89) {

            return "Оценка B ";
        } else if (x >= 90 && x <= 100) {

            return "Оценка A ";
        } else {
            return "Невозможно определить оценку ";

        }

    }
}
