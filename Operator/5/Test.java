import com.company.Task5;
import org.junit.Assert;

public class Test {

    @org.junit.Test
    public void test() {
        Task5 task3 = new Task5();
        String result = task3.rating(10);
        Assert.assertEquals("Оценка F ", result);

        String result2 = task3.rating(30);
        Assert.assertEquals("Оценка E ", result2);

        String result3 = task3.rating(50);
        Assert.assertEquals("Оценка D ", result3);

        String result4 = task3.rating(70);
        Assert.assertEquals("Оценка C ", result4);

        String result5 = task3.rating(80);
        Assert.assertEquals("Оценка B ", result5);

        String result6 = task3.rating(99);
        Assert.assertEquals("Оценка A ", result6);

    }
}

