import com.company.Task4;
import org.junit.Assert;

public class Test {

    @org.junit.Test
    public void test() {
        Task4 task4 = new Task4();
        int result = task4.max(1, 1, 1);
        Assert.assertEquals(6, result);

        int result1 = task4.max(0, 0, 0);
        Assert.assertEquals(3, result1);

        int result2 = task4.max(-1, 0, 1);
        Assert.assertEquals(3, result2);

        int result3 = task4.max(5, 5, 5);
        Assert.assertEquals(128, result3);


    }
}