package com.company;

import java.util.Scanner;

public class Task2 {

    public static void main(String[] args) {

        System.out.println("Определить какой четверти принадлежит точка с координатами (х,у)");
        System.out.println(chec(5, 5));

    }

    public static String chec(int x, int y) {
        System.out.println("x="+x+" y="+y+"");
        if ((x > 0) && (y > 0)) {

            return "Точка находится в I четверти";

        } else if ((x < 0) && (y > 0)) {
            return "Точка находится в II четверти";
        } else if ((x < 0) && (y < 0)) {
            return "Точка находится в III четверти";
        } else if ((x > 0) && (y < 0)) {
            return "Точка находится в IV четверти";
        } else if ((x == 0) && (y == 0)) {
            return "Точка находится в центре";

        } else if (x == 0) {
            return "Точка находится на оси 'x'";

        } else if (y == 0) {
            return "Точка находится на оси 'y'";

        }
        return "";
    }

}

