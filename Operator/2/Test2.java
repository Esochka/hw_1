import com.company.Task2;
import org.junit.Assert;
public class Test2 {
    @org.junit.Test
    public void task2() {
        Task2 task2 = new Task2();

        String res1 = task2.chec(52,5);
        Assert.assertEquals("Точка находится в I четверти",res1);

        String res2 = task2.chec(-52,5);
        Assert.assertEquals("Точка находится в II четверти",res2);

        String res3 = task2.chec(-52,-5);
        Assert.assertEquals("Точка находится в III четверти",res3);

        String res4 = task2.chec(52,-5);
        Assert.assertEquals("Точка находится в IV четверти",res4);

        String res5 = task2.chec(0,0);
        Assert.assertEquals("Точка находится в центре",res5);

        String res6 = task2.chec(0,5);
        Assert.assertEquals("Точка находится на оси 'x'",res6);

        String res7 = task2.chec(52,0);
        Assert.assertEquals("Точка находится на оси 'y'",res7);
    }
}







