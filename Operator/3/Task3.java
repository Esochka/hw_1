package com.company;

public class Task3 {

    public static void main(String[] args) {

        System.out.println("Найти суммы только положительных из трех чисел");
        System.out.println(sum(5, -5, 5));


    }

    public static int sum(int x, int y, int z) {
        System.out.println("x="+x+" y="+y+" z="+z+"");
        int sum = 0;
        if (x > 0 && y > 0 && z > 0) {
            sum = x+y+z;
            return sum;
        } else {
            int[] array = {x, y, z};

            for (int i : array) {
                if (i > 0) {
                    sum = sum + i;
                }

            }
            return sum;

        }


    }
}