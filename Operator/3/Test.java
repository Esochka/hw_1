import com.company.Task3;
import org.junit.Assert;

public class Test {

    @org.junit.Test
    public void test() {
        Task3 task3 = new Task3();
        int result = task3.sum(5, 5, 5);
        Assert.assertEquals(15, result);

        int result1 = task3.sum(-5, 5, 5);
        Assert.assertEquals(10, result1);

        int result2 = task3.sum(5, -5, 5);
        Assert.assertEquals(10, result2);

        int result3 = task3.sum(5, 5, -5);
        Assert.assertEquals(10, result3);

        int result4 = task3.sum(-5, -5, 5);
        Assert.assertEquals(5, result4);

        int result5 = task3.sum(5, -5, -5);
        Assert.assertEquals(5, result5);

        int result6 = task3.sum(-5, 5, -5);
        Assert.assertEquals(5, result6);

        int result7 = task3.sum(-5, -5, -5);
        Assert.assertEquals(0, result7);


    }


}
