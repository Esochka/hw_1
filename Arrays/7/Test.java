import com.company.Task7;
import org.junit.Assert;

public class Test {


    @org.junit.Test
    public void test() {
        Task7 task1 = new Task7();
        int result = task1.count(new int[]{1, 2, 3, -4, 5, 6, 7});
        Assert.assertEquals(4, result);
        int res = task1.count(new int[]{1, 2, 3, -4, 5, -6, 7,9,10});
        Assert.assertEquals(5, res);
    }
}
