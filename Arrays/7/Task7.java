package com.company;

public class Task7 {

    public static void main(String[] args) {

        System.out.println("Посчитать количество нечетных элементов массива");
        System.out.println(count(new int[]{1, 2, 3, -4, 5, 6, 7}));

    }

    public static int count(int[] list) {
        for (int i = 0; i < list.length; i++) {
            System.out.print(list[i]);
        }
        System.out.println("\n");

        int k = 0;
        for (int i = 0; i < list.length; i++)
        {
            if (i % 2 == 0) {
                k=k+1;
            }

        }

        return k;

    }
}
