import com.company.Task10;
import org.junit.Assert;

public class Test2 {


    @org.junit.Test
    public void test() {
        Task10 task1 = new Task10();
        String result = task1.Shell(new int[]{1, 2, 3, -4, 5, 6, 7});
        Assert.assertEquals("765321-4", result);
        String res = task1.Shell(new int[]{1, 2, 3, -4, 5, -6, 7});
        Assert.assertEquals("75321-4-6", res);
    }
}
