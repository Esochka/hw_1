package com.company;

import java.util.Scanner;
import java.util.Arrays;


public class Task10 {

    public static void main(String[] args) {

        System.out.println("Отсортировать массив (Quick, Merge, Shell, Heap)");
        System.out.println(Quick(new int[]{1, 2, 3, -4, 5, 6, 7}, 0));

        System.out.println(Merge(new int[]{1, 2, 3, -4, 5, 6, 7}));

        System.out.println(Shell(new int[]{1, 2, 3, -4, 5, 6, 7}));

        System.out.println(Heap(new int[]{1, 2, 3, -4, 5, 6, 7}));

    }



    public static String Quick(int[] array, int low) {

        String rev = "";
        int high = array.length - 1;
        if (array.length == 0)
            return "";

        int middle = low + (high - low) / 2;
        int opora = array[middle];

        int i = low, j = high;
        while (i <= j) {
            while (array[i] > opora) {
                i++;
            }

            while (array[j] < opora) {
                j--;
            }

            if (i <= j) {
                int temp = array[i];
                array[i] = array[j];
                array[j] = temp;
                i++;
                j--;
            }
        }
        if (low < j)
            Quick(array, low);

        if (high > i)
            Quick(array, i);


        for (i = 0; i < array.length; i++) {
            rev = rev + array[i];

        }

        return rev;

    }


    public static String Merge(int[] source) {
        String rev = "";
        if (source.length > 1) {
            int mid = source.length / 2;
            int[] lefthalf = Arrays.copyOfRange(source, 0, mid),
                    righthalf = Arrays.copyOfRange(source, mid, source.length);
            Merge(lefthalf);
            Merge(righthalf);

            int i = 0;
            int j = 0;
            int k = 0;
            while (i < lefthalf.length && j < righthalf.length) {
                if (lefthalf[i] > righthalf[j]) {
                    source[k] = lefthalf[i];
                    i++;
                } else {
                    source[k] = righthalf[j];
                    j++;
                }
                k++;
            }
            while (i < lefthalf.length) {
                source[k] = lefthalf[i];
                i++;
                k++;
            }
            while (j < righthalf.length) {
                source[k] = righthalf[j];
                j++;
                k++;
            }
        }

        for (int i = 0; i < source.length; i++) {
            rev = rev + source[i];

        }

        return rev;

    }


    public static String Shell(int[] arr) {
        String rev = "";
        for (int inc = arr.length / 2; inc >= 1; inc = inc / 2)
            for (int step = 0; step < inc; step++)

                for (int i = step; i < arr.length - 1; i += inc) {
                    int tmp;
                    for (int j = Math.min(i + inc, arr.length - 1); j - inc >= 0; j = j - inc)
                        if (arr[j - inc] < arr[j]) {
                            tmp = arr[j];
                            arr[j] = arr[j - inc];
                            arr[j - inc] = tmp;
                        } else break;
                }

        for (int i = 0; i < arr.length; i++) {
            rev = rev + arr[i];

        }

        return rev;
    }


    public static String Heap(int arr[]) {
        String rev = "";
        int n = arr.length;

        for (int i = n / 2 - 1; i >= 0; i--)
            heapify(arr, n, i);


        for (int i = n - 1; i >= 0; i--) {

            int temp = arr[0];
            arr[0] = arr[i];
            arr[i] = temp;


            heapify(arr, i, 0);
        }
        for (int i = 0; i < arr.length; i++) {
            rev = rev + arr[i];

        }

        return rev;
    }


    public static void heapify(int arr[], int n, int i) {
        int largest = i;
        int l = 2 * i + 1;
        int r = 2 * i + 2;


        if (l < n && arr[l] < arr[largest])
            largest = l;


        if (r < n && arr[r] < arr[largest])
            largest = r;

        if (largest != i) {
            int swap = arr[i];
            arr[i] = arr[largest];
            arr[largest] = swap;


            heapify(arr, n, largest);
        }
    }


}










