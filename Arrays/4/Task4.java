package com.company;

public class Task4 {

    public static void main(String[] args) {

        System.out.println("Найти индекс максимального элемента массива");
        System.out.println(maxIndex(new int[]{1, 2, 3, -4, 5, 6, 7}));

    }

    public static int maxIndex(int[] list) {
        for (int i = 0; i < list.length; i++) {
            System.out.print(list[i]);
        }
        System.out.println("\n");
        int k = 0;
        int max = list[0];
        for (int i = 0; i < list.length; i++) {
            if (list[i] > max) {
                max = list[i];
                k = i;
            }

        }


        return k;
    }
}
