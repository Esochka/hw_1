import com.company.Task4;
import org.junit.Assert;

public class Test {


    @org.junit.Test
    public void test() {
        Task4 task1 = new Task4();
        int result = task1.maxIndex(new int[]{1, 2, 3, -4, 5, 6, 7});
        Assert.assertEquals(6, result);
        int res = task1.maxIndex(new int[]{1, 2, 3, 10, 5, -6, 7});
        Assert.assertEquals(3, res);
    }
}
