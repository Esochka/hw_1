import com.company.Task1;
import org.junit.Assert;

public class Test {


    @org.junit.Test
    public void test() {
        Task1 task1 = new Task1();
        int result = task1.min(new int[]{1, 2, 3, -4, 5, 6, 7});
        Assert.assertEquals(-4, result);
        int res = task1.min(new int[]{1, 2, 3, -4, 5, -6, 7});
        Assert.assertEquals(-6, res);
    }
}
