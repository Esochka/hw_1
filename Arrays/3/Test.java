import com.company.Task3;
import org.junit.Assert;

public class Test {


    @org.junit.Test
    public void test() {
        Task3 task1 = new Task3();
        int result = task1.minIndex(new int[]{1, 2, 3, -4, 5, 6, 7});
        Assert.assertEquals(3, result);
        int res = task1.minIndex(new int[]{1, 2, 3, -4, 5, -6, 7});
        Assert.assertEquals(5, res);
    }
}
