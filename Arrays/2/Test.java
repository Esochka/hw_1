import com.company.Task2;
import org.junit.Assert;

public class Test {


    @org.junit.Test
    public void test() {
        Task2 task1 = new Task2();
        int result = task1.max(new int[]{1, 2, 3, -4, 5, 6, 7});
        Assert.assertEquals(7, result);
        int res = task1.max(new int[]{1, 2, 3, 10, 5, -6, 7});
        Assert.assertEquals(10, res);
    }
}
