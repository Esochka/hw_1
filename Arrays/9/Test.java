import com.company.Task9;
import org.junit.Assert;

public class Test {


    @org.junit.Test
    public void test() {
        Task9 task1 = new Task9();
        String result = task1.Bubble(new int[]{1, 2, 3, -4, 5, 6, 7});
        Assert.assertEquals("765321-4", result);
        String res = task1.Bubble(new int[]{1, 2, 3, -4, 5, -6, 7});
        Assert.assertEquals("75321-4-6", res);
    }
}
