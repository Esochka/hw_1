package com.company;

import java.util.Scanner;
import java.util.Arrays;

public class Task9 {

    public static void main(String[] args) {

        System.out.println("Отсортировать массив (пузырьком (Bubble), выбором (Select), вставками (Insert))");
        System.out.println(Bubble(new int[]{1, 2, 3, -4, 5, 6, 7}));
        System.out.print("\n");
        System.out.println(Select(new int[]{1, 2, 3, -4, 5, 6, 7}));
        System.out.print("\n");
        System.out.println(Insert(new int[]{1, 2, 3, -4, 5, 6, 7}));
    }

    public static String Bubble(int list[]) {
        for (int i = 0; i < list.length; i++) {
            System.out.print(list[i]);
        }
        System.out.print("\n");
        String rev = "";
        int i, j, temp;
        boolean flag = true;
        while (flag) {
            flag = false;
            for (j = 0; j < list.length - 1; j++) {
                if (list[j] < list[j + 1]) {
                    temp = list[j];
                    list[j] = list[j + 1];
                    list[j + 1] = temp;
                    flag = true;
                }
            }
        }
        for (i = 0; i < list.length; i++) {
            rev = rev + list[i];

        }

        return rev;


    }

    public static String Select(int list[]) {
        for (int i = 0; i < list.length; i++) {
            System.out.print(list[i]);
        }
        System.out.print("\n");
        int i, j, temp;
        String rev = "";
        for (i = 0; i < list.length; i++) {
            int pos = i;
            temp = list[i];
            for (j = i + 1; j < list.length; j++) {
                if (list[j] > temp) {
                    pos = j;
                    temp = list[j];
                }
            }
            list[pos] = list[i];
            list[i] = temp;
        }

        for (i = 0; i < list.length; i++) {
            rev = rev + list[i];

        }

        return rev;


    }

    public static String Insert(int list[]) {
        for (int i = 0; i < list.length; i++) {
            System.out.print(list[i]);
        }
        System.out.print("\n");
        int i, j, temp;
        String rev = "";
        for (i = 0; i < list.length; i++) {

            temp = list[i];


            for (j = i - 1; j >= 0; j--) {

                if (temp > list[j]) {
                    list[j + 1] = list[j];
                } else {

                    break;
                }
            }

            list[j + 1] = temp;
        }
        for (i = 0; i < list.length; i++) {
            rev = rev + list[i];

        }

        return rev;
    }


}

