import com.company.Task8;
import org.junit.Assert;

public class Test {


    @org.junit.Test
    public void test() {
        Task8 task1 = new Task8();
        String result = task1.revers(new int[]{1, 2, 3, -4, 5, 6, 7});
        Assert.assertEquals("567-4123", result);
        String res = task1.revers(new int[]{1, 2, 3, -4, 5, -6, 7});
        Assert.assertEquals("5-67-4123", res);
    }
}
