import com.company.Task6;
import org.junit.Assert;

public class Test {


    @org.junit.Test
    public void test() {
        Task6 task1 = new Task6();
        String result = task1.revers(new int[]{1, 2, 3, -4, 5, 6, 7});
        Assert.assertEquals("765-4321", result);
        String res = task1.revers(new int[]{1, 2, 3, -4, 5, -6, 7});
        Assert.assertEquals("7-65-4321", res);
    }
}
