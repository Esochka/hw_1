import com.company.Task5;
import org.junit.Assert;

public class Test {


    @org.junit.Test
    public void test() {
        Task5 task1 = new Task5();
        int result = task1.sum(new int[]{1, 2, 3, -4, 5, 6, 7});
        Assert.assertEquals(16, result);
        int res = task1.sum(new int[]{1, 2, 5, 0, 5, -6, 7});
        Assert.assertEquals(18, res);
    }
}
